<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

class phoneUpdateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $phone;

    private $apiUser;
    private $dbh;
    private $sth;

    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $mockApiUsername = '';

    protected function setUp()
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->mockApiUsername = '';

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('getUsername'))
            ->getMock();

        $this->apiUser->expects($this->once())->method('getUsername')
            ->will($this->returnCallback(array($this, 'getUsernameMock')));

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')),
                $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare'))
            ->getMock();

        $this->dbh->method('prepare')->willReturn($this->sth);

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->phone = new \MiamiOH\RestngPhoneService\Services\Phone();

        $this->phone->setDatabase($db);
        $this->phone->setApiUser($this->apiUser);

    }

    public function testPhoneUpdateNoStatusIsActive()
    {

        $this->mockApiUsername = 'MUWS_SEC';

        $model = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => null,
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => null,
        ];

        $result = $this->phone->update($model);

        $this->assertEquals($model, $result);

        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'],
            'PIDM is bound to correct PIDM');
        $this->assertEquals($model['phoneType'], $this->boundValues[':P_TELE_CODE'],
            'Phone type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER_ID'],
            'Active user is bound to correct value');
        $this->assertNull($this->boundValues[':P_STATUS_IND'],
            'Phone status is active');

    }

    public function testPhoneUpdateInactive()
    {

        $this->mockApiUsername = 'MUWS_SEC';

        $model = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => null,
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => null,
            'status' => 'inactive',
        ];

        $result = $this->phone->update($model);

        $this->assertEquals($model, $result);

        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'],
            'PIDM is bound to correct PIDM');
        $this->assertEquals($model['phoneType'], $this->boundValues[':P_TELE_CODE'],
            'Phone type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER_ID'],
            'Active user is bound to correct value');
        $this->assertEquals('I', $this->boundValues[':P_STATUS_IND'],
            'Phone status is inactive');

    }

    public function testPhoneUpdateActive()
    {

        $this->mockApiUsername = 'MUWS_SEC';

        $model = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => null,
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => null,
            'status' => 'active',
        ];

        $result = $this->phone->update($model);

        $this->assertEquals($model, $result);

        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'],
            'PIDM is bound to correct PIDM');
        $this->assertEquals($model['phoneType'], $this->boundValues[':P_TELE_CODE'],
            'Phone type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER_ID'],
            'Active user is bound to correct value');
        $this->assertNull($this->boundValues[':P_STATUS_IND'],
            'Phone status is active');

    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }

}
