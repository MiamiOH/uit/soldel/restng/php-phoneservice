<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

class phoneCreateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $phone;

    private $apiUser;
    private $dbh;
    private $sth;
    private $phoneHelper;
    private $phoneUtil;

    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $mockApiUsername = '';

    protected function setUp()
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->mockApiUsername = '';

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('getUsername'))
            ->getMock();

        $this->apiUser->method('getUsername')
            ->will($this->returnCallback(array($this, 'getUsernameMock')));

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')),
                $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare', 'queryall_array'))
            ->getMock();

        $this->dbh->method('prepare')->willReturn($this->sth);

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->phone = new \MiamiOH\RestngPhoneService\Services\Phone();

        $this->phone->setDatabase($db);
        $this->phone->setApiUser($this->apiUser);
        $this->phone->setPhoneHelper($this->phoneHelper);

    }

    public function testPhoneCreate()
    {

        $this->mockApiUsername = 'MUWS_SEC';

        $model = [
            'pidm' => '123456',
            'phoneType' => 'CELL',
            'phoneDesc' => '',
            'activityDate' => null,
            'isPrimary' => true,
            'phoneNumber' => '+15134567890',
            'phoneNumberNational' => '',
            'phoneNumberInternational' => '',
            'sequenceNumber' => null,
            'status' => '',
        ];

        $this->records = [
            [
                'sprtele_pidm' => '123456',
                'sprtele_tele_code' => 'CELL',
                'stvtele_desc' => 'Mobile',
                'sprtele_activity_date' => '2016-08-01',
                'sprtele_primary_ind' => 'Y',
                'sprtele_phone_area' => '513',
                'sprtele_phone_number' => '4567890',
                'sprtele_phone_ext' => '',
                'sprtele_intl_access' => '',
                'sprtele_seqno' => '1',
                'sprtele_status_ind' => null,
                'row_id' => 'asldkjflasdf'
            ],
        ];
        
        $parsedPhoneNumber = new \libphonenumber\PhoneNumber();
        $parsedPhoneNumber->setExtension(null);

        $this->mockParse = [
            'phoneNumber' => '+15134567890',

        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('parse')
            ->with('+15134567890')
            ->willReturn($parsedPhoneNumber);

        $this->phoneUtil->method('isValidNumber')
            ->with($parsedPhoneNumber)
            ->willReturn('1');

        $this->phoneUtil->method('format')
            ->with($parsedPhoneNumber)
            ->willReturn('(513) 456-7890');


        $result = $this->phone->create($model);


        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'],
            'PIDM is bound to correct PIDM');
        $this->assertEquals($model['phoneType'], $this->boundValues[':P_TELE_CODE'],
            'Phone type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER_ID'],
            'Active user is bound to correct value');

        // Test makeModelForPostResponse query
        $this->assertTrue(in_array($model['pidm'], $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'FROM SPRTELE') !== false,
            'Query contains from sprtele');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false,
            'Query contains WHERE');

        // Test Post response model
        $this->assertEquals($result['phoneDesc'], 'Mobile',
            'Phone description is set to Mobile');
        $this->assertEquals($result['phoneNumberNational'], '(513) 456-7890',
            'Phone national is set to (513) 456-7890');
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Missing required Phone Number
     */
    public function testPhoneCreateMissingPhoneNumber()
    {

        $this->mockApiUsername = 'MUWS_SEC';

        $model = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'phoneDesc' => '',
            'activityDate' => null,
            'isPrimary' => true,
            'phoneNumber' => '',
            'phoneNumberNational' => '',
            'phoneNumberInternational' => '',
            'sequenceNumber' => null,
            'status' => '',
        ];

        $result = $this->phone->create($model);

    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }

}
