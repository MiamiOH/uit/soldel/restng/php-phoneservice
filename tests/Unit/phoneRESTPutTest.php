<?php

class phoneRESTPutTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $phoneREST;

    private $api;
    private $phone;

    private $mockResponse = '';
    private $updateModel = [];
    private $mockPayload = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $this->mockResponse = '';
        $this->updateModel = [];
        $this->mockPayload = [];

        //set up the mock api:
        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->phone = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\Phone')
            ->setMethods(array('update'))
            ->getMock();

        $this->phone->method('update')
            ->with($this->callback(array($this, 'updateWith')))
            ->will($this->returnCallback(array($this, 'updateMock')));

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        //set up the service with the mocked out resources:
        $this->phoneREST = new \MiamiOH\RestngPhoneService\Services\PhoneREST();

        $this->phoneREST->setLogger();
        $this->phoneREST->setApp($this->api);
        $this->phoneREST->setPhone($this->phone);
        $this->phoneREST->setBannerUtil($bannerUtil);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Create an phone record
     *
     *	Expected Return: 200 OK Response
     */
    public function testUpdatePhone()
    {

        $this->mockPayload = [
            [
                'pidm' => 567890,
                'phoneType' => 'CELL',
                'activityDate' => null,
                'isPrimary' => true,
                'phoneNumber' => '+15131234567',
                'sequenceNumber' => 1,
                'status' => 'active',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn(567890);

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->putPhoneCollection();

        $payload = $response->getPayload();

        /***Make assertions***/
        // Check the status and general response
        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(567890, $this->updateModel['pidm'],
            'Create phone called for expected pidm');

    }

    public function testUpdatePhoneNotFound()
    {
        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = JONESL'")));

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->putPhoneCollection();

        $this->assertEquals(MiamiOH\RESTng\App::API_NOTFOUND,
            $response->getStatus());
    }

    public function getDataMock()
    {
        return $this->mockPayload;
    }

    public function updateWith($subject)
    {
        $this->updateModel = $subject;

        return true;
    }

    public function updateMock()
    {
        return $this->mockResponse;
    }
}