<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

class phoneReadTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $phone;

    private $dbh;

    private $phoneHelper;
    private $phoneUtil;

    private $records = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp()
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->phone = new \MiamiOH\RestngPhoneService\Services\Phone();

        $this->phone->setDatabase($db);
        $this->phone->setPhoneHelper($this->phoneHelper);

    }

    public function testPhoneRead()
    {

        $pidm = '110518';

        $this->records = [
            [
                'sprtele_pidm' => '110518',
                'sprtele_tele_code' => 'MA',
                'stvtele_desc' => 'Permanent Mailing Phone',
                'sprtele_activity_date' => '2016-08-01',
                'sprtele_primary_ind' => 'Y',
                'sprtele_phone_area' => '234',
                'sprtele_phone_number' => '1234567',
                'sprtele_phone_ext' => '',
                'sprtele_intl_access' => '',
                'sprtele_seqno' => '1',
                'sprtele_status_ind' => null,
                'row_id' => 'asldkjflasdf'
            ],
            [
                'sprtele_pidm' => '110518',
                'sprtele_tele_code' => 'LO',
                'stvtele_desc' => 'Local (Off-Campus) Phone',
                'sprtele_activity_date' => '2016-08-01',
                'sprtele_primary_ind' => 'Y',
                'sprtele_phone_area' => '654',
                'sprtele_phone_number' => '6544567',
                'sprtele_phone_ext' => '',
                'sprtele_intl_access' => '',
                'sprtele_seqno' => '2',
                'sprtele_status_ind' => null,
                'row_id' => 'asldkjflasdf'
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->phone->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'FROM SPRTELE') !== false,
            'Query contains from sprtele');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false,
            'Query contains WHERE');
        $this->assertTrue(strpos($this->queryString, 'ORDER BY') !== false,
            'Query contains ORDER BY');

        $this->assertEquals(2, count($models));

        $record = $this->records[0];
        $model = $models[0];

        $expectedPhoneNumber = '+1' . preg_replace('/[\s\(\)+-]/', '',
                $record['sprtele_phone_area'] .
                $record['sprtele_phone_number'] .
                $record['sprtele_phone_ext']);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals($expectedPhoneNumber, $model['phoneNumber']);
        $this->assertEquals('active', $model['status']);
    }

    public function testPhoneReadByType()
    {

        $pidm = '110518';
        $types = ['MA', 'CELL'];

        $this->records = [
            [
                'sprtele_pidm' => '110518',
                'sprtele_tele_code' => 'CELL',
                'stvtele_desc' => 'Mobile',
                'sprtele_activity_date' => '2016-08-01',
                'sprtele_primary_ind' => 'Y',
                'sprtele_phone_area' => '234',
                'sprtele_phone_number' => '1234567',
                'sprtele_phone_ext' => '',
                'sprtele_intl_access' => '',
                'sprtele_seqno' => '1',
                'sprtele_status_ind' => null,
                'row_id' => 'asldkjflasdf'
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->phone->filterType($types)->read($pidm);

        foreach ($types as $type) {
            $this->assertTrue(in_array($type, $this->queryParams));
        }
        $this->assertTrue(stripos($this->queryString,
                'sprtele_tele_code in') !== false,
            'Query contains sprtele_tele_code in condition');

    }

    public function testPhoneReadByStatus()
    {

        $pidm = '110518';
        $status = 'active';

        $this->records = [
            [
                'sprtele_pidm' => '110518',
                'sprtele_tele_code' => 'CELL',
                'stvtele_desc' => 'Mobile',
                'sprtele_activity_date' => '2016-08-01',
                'sprtele_primary_ind' => 'Y',
                'sprtele_phone_area' => '234',
                'sprtele_phone_number' => '1234567',
                'sprtele_phone_ext' => '',
                'sprtele_intl_access' => '',
                'sprtele_seqno' => '1',
                'sprtele_status_ind' => null,
                'row_id' => 'asldkjflasdf'
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->phone->filterStatus($status)->read($pidm);

        $this->assertTrue(stripos($this->queryString,
                'sprtele_status_ind is null') !== false,
            'Query contains sprtele_status_ind in condition');

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }


}
