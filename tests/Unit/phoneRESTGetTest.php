<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

class phoneRESTGetTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $phoneREST;

    private $api;
    private $phone;
    private $request;

    private $phoneData = [];

    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    private $mockReadResponse = [];
    private $readPidm = '';
    private $filterType = '';
    private $filterStatus = '';

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $this->phoneData = [];

        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];
        $this->mockReadResponse = [];
        $this->readPidm = '';
        $this->filterType = '';
        $this->filterStatus = '';

        //set up the mock api:
        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->phone = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\Phone')
            ->setMethods(array('read', 'filterType', 'filterStatus'))
            ->getMock();

        $this->phone->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));

        $this->phone->method('filterType')
            ->with($this->callback(array($this, 'filterTypeWith')))
            ->will($this->returnSelf());

        $this->phone->method('filterStatus')
            ->with($this->callback(array($this, 'filterStatusWith')))
            ->will($this->returnSelf());

        //set up the service with the mocked out resources:
        $this->phoneREST = new \MiamiOH\RestngPhoneService\Services\PhoneREST();

        $this->phoneREST->setLogger();
        $this->phoneREST->setApp($this->api);
        $this->phoneREST->setBannerUtil($bannerUtil);
        $this->phoneREST->setPhone($this->phone);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetPhoneREST()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => '2016-08-01',
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->getPhone();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);

    }

    public function testGetPhoneRESTType()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => '2016-08-01',
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->request->method('getOptions')
            ->willReturn(['phoneType' => 'CELL']);

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->getPhone();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('CELL', $this->filterType);

    }

    public function testGetPhoneRESTStatus()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'phoneType' => 'CELL',
            'activityDate' => '2016-08-01',
            'isPrimary' => true,
            'phoneNumber' => '+15131234567',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->request->method('getOptions')
            ->willReturn(['status' => 'active']);

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->getPhone();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('active', $this->filterStatus);

    }

    public function testGetPhoneRESTInvalidId()
    {

        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = DOEJ'")));

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->getPhone();

        $this->assertEquals(MiamiOH\RESTng\App::API_NOTFOUND,
            $response->getStatus());

    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;

        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }

    public function readWith($subject)
    {
        $this->readPidm = $subject;

        return true;
    }

    public function readMock()
    {
        return $this->mockReadResponse;
    }

    public function filterTypeWith($subject)
    {
        $this->filterType = $subject;

        return true;
    }

    public function filterStatusWith($subject)
    {
        $this->filterStatus = $subject;

        return true;
    }

}
