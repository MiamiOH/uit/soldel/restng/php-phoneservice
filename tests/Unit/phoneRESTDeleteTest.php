<?php

class phoneRESTDeleteTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $phoneREST;

    private $request;
    private $api;
    private $phone;
    private $bannerId;

    private $mockData = [];
    private $deleteModel = [];
    private $deleteResult = null;

    protected function setUp()
    {

        $this->mockData = [];
        $this->deleteModel = [];
        $this->deleteResult = null;

        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->phone = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\Phone')
            ->setMethods(array('delete'))
            ->getMock();

        $this->phone->method('delete')
            ->with($this->callback(array($this, 'deleteWith')))
            ->will($this->returnCallback(array($this, 'deleteMock')));

        $this->phoneREST = new \MiamiOH\RestngPhoneService\Services\PhoneREST();

        $this->phoneREST->setApp($this->api);
        $this->phoneREST->setPhone($this->phone);
        $this->phoneREST->setBannerUtil($bannerUtil);

    }

    public function testDeletePhone()
    {
        $this->mockData = [
            [
                'pidm' => '123456',
                'activityDate' => '',
                'phoneNumber' => '5136646124',
                'sequenceNumber' => '1',
                'phoneType' => 'LO',
                'isPrimary' => '',
                'status' => '',

            ],
        ];

        $this->deleteResult = true;

        $this->bannerId->method('getPidm')->willReturn('123456');

        $this->phoneREST->setRequest($this->request);

        $resp = $this->phoneREST->deletePhoneCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    }

    public function getDataMock()
    {
        return $this->mockData;
    }

    public function deleteWith($subject)
    {
        $this->deleteModel = $subject;

        return true;
    }

    public function deleteMock()
    {
        return $this->deleteResult;
    }

    public function deletePhoneResponseMock()
    {
        $payload = [
            'data' => [
                [
                    'pidm' => '123456',
                    'code' => 200,
                    'message' => 'DELETE Success'
                ]
            ],
            'status' => 200,
            'error' => 'false'
        ];

        return $payload;
    }
}