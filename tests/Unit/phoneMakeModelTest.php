<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

class phoneMakeModelTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $phone;

    private $phoneHelper;
    private $phoneUtil;

    protected function setUp()
    {

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->phone = new \MiamiOH\RestngPhoneService\Services\Phone();

        $this->phone->setPhoneHelper($this->phoneHelper);

    }

    public function testPhoneMakeModelUS()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '513',
            'sprtele_phone_number' => '1234567',
            'sprtele_intl_access' => '',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $this->phoneUtil->method('parse')->will($this->returnSelf());
        $this->phoneUtil->method('isValidNumber')->willReturn(true);
        $this->phoneUtil->method('format')->will($this->onConsecutiveCalls(
            '(513) 123-4567', '+1 513-123-4567'
        ));
        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals('+1' . $record['sprtele_phone_area'] . $record['sprtele_phone_number'],
            $model['phoneNumber']);
        $this->assertEquals('(513) 123-4567', $model['phoneNumberNational']);
        $this->assertEquals('+1 513-123-4567', $model['phoneNumberInternational']);

    }
// Not using phone extension.
    /*public function testPhoneMakeModelUSWithExtension()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '513',
            'sprtele_phone_number' => '1234567',
            'sprtele_intl_access' => '',
            'sprtele_phone_ext' => '543',
            'sprtele_status_ind' => '',
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'], $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals('+1' . $record['sprtele_phone_area'] . $record['sprtele_phone_number'] .
            ' ' . $record['sprtele_phone_ext'], $model['phoneNumber']);
        $this->assertEquals('(513) 123-4567', $model['phoneNumberNational']);
        $this->assertEquals('+1 513-123-4567', $model['phoneNumberInternational']);

    }*/

    public function testPhoneMakeModelInternational()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '',
            'sprtele_phone_number' => '',
            'sprtele_intl_access' => '+85131234567',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals($record['sprtele_intl_access'], $model['phoneNumber']);

    }

    public function testPhoneMakeModelInactive()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '513',
            'sprtele_phone_number' => '1234567',
            'sprtele_intl_access' => '',
            'sprtele_status_ind' => 'I',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals('inactive', $model['status']);

    }

    public function testPhoneMakeModelNotPrimary()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => '',
            'sprtele_phone_area' => '513',
            'sprtele_phone_number' => '1234567',
            'sprtele_intl_access' => '',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertFalse($model['isPrimary']);

    }

    /*
     * Case1: sprtele_intl_access has country code, sprtele_phone_area has area code
     * and sprtele_phone_number has phone number.
     * makeModelFromRecord should concatenate all three fields and update phoneNumber field in model.
     */
    public function testPhoneMakeModelCase1()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '7825',
            'sprtele_phone_number' => '603626',
            'sprtele_intl_access' => '+44',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals($record['sprtele_intl_access'] . $record['sprtele_phone_area'] . $record['sprtele_phone_number'],
            $model['phoneNumber']);

    }

    /*
    * Case2: sprtele_intl_access has country name(string), sprtele_phone_area has area code
    * and sprtele_phone_number has phone number.
    * makeModelFromRecord should check if sprtele_intl_access is set and contains string and
     * concatenate sprtele_phone_area , sprtele_phone_number and update phoneNumber field in model.
    */
    public function testPhoneMakeModelCase2()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '7825',
            'sprtele_phone_number' => '603626',
            'sprtele_intl_access' => 'China',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals('+' . $record['sprtele_phone_area'] . $record['sprtele_phone_number'],
            $model['phoneNumber']);

    }

    /*
     * Case3: sprtele_intl_access has international phone number and sprtele_phone_area+sprtele_phone_number has USA phone number
     * then phone number should hold international number?
     */
    public function testPhoneMakeModelCase3()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '7825',
            'sprtele_phone_number' => '603626',
            'sprtele_intl_access' => '+85131234567',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals($record['sprtele_intl_access'], $model['phoneNumber']);

    }

    /*
     * Case4: International phone number is split and stored in sprtele_phone_area and sprtele_phone_number
     */
    public function testPhoneMakeModelCase4()
    {
        $record = [
            'sprtele_pidm' => '123456',
            'sprtele_tele_code' => 'CELL',
            'stvtele_desc' => 'Mobile',
            'sprtele_activity_date' => '2016-08-01',
            'sprtele_seqno' => '1',
            'sprtele_primary_ind' => 'Y',
            'sprtele_phone_area' => '091',
            'sprtele_phone_number' => '9620224164',
            'sprtele_intl_access' => '',
            'sprtele_status_ind' => '',
            'row_id' => 'asldkjflasdf'
        ];

        $model = $this->phone->makeModelFromRecord($record);

        $this->assertEquals($record['sprtele_pidm'], $model['pidm']);
        $this->assertEquals($record['sprtele_tele_code'], $model['phoneType']);
        $this->assertEquals($record['sprtele_activity_date'],
            $model['activityDate']);
        $this->assertEquals($record['sprtele_seqno'], $model['sequenceNumber']);
        $this->assertTrue($model['isPrimary']);
        $this->assertEquals('active', $model['status']);

        $this->assertEquals('+' . $record['sprtele_phone_area'] . $record['sprtele_phone_number'],
            $model['phoneNumber']);

    }

}
