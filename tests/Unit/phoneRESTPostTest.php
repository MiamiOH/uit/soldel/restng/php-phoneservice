<?php

class phoneRESTPostTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $phoneREST;

    private $api;
    private $phone;

    private $mockModel = [];
    private $createModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $this->mockModel = [];
        $this->createModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->phone = $this->getMockBuilder('\MiamiOH\RestngPhoneService\Services\Phone')
            ->setMethods(array('create'))
            ->getMock();

        $this->phone->method('create')
            ->with($this->callback(array($this, 'createWith')))
            ->will($this->returnCallback(array($this, 'createMockModel')));

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        //set up the service with the mocked out resources:
        $this->phoneREST = new \MiamiOH\RestngPhoneService\Services\PhoneREST();

        $this->phoneREST->setLogger();
        $this->phoneREST->setApp($this->api);
        $this->phoneREST->setPhone($this->phone);
        $this->phoneREST->setBannerUtil($bannerUtil);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testCreatePhoneREST()
    {
        $this->mockModel = [
            [
                'pidm' => 567890,
                'phoneType' => 'CELL',
                'activityDate' => null,
                'isPrimary' => true,
                'phoneNumber' => '+15131234567',
                'sequenceNumber' => 1,
                'status' => 'active',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn(567890);

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->postPhoneCollection();

        $payload = $response->getPayload();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $response->getStatus());

    }

    public function testCreatePhoneNotFound()
    {
        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = JONESL'")));

        $this->phoneREST->setRequest($this->request);

        $response = $this->phoneREST->postPhoneCollection();

        $this->assertEquals(MiamiOH\RESTng\App::API_NOTFOUND,
            $response->getStatus());
    }

    public function createWith($subject)
    {
        $this->createModel = $subject;

        return true;
    }

    public function createMockModel()
    {
        return $this->createModel;
    }

    public function getDataMock()
    {
        return $this->mockModel;
    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;

        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }

}