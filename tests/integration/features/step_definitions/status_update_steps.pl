#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given qr/a collection of phone records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                     'pidm' => '567890',
                     'phoneType' => 'LO',
                     'phoneDesc' => '',
                     'activityDate' =>'',
                     'phoneNumber' => '+12247780086',
                     'phoneNumberNational' => '',
                     'phoneNumberInternational' => '',
                     'sequenceNumber' => '1',
                     'isPrimary' => '',
                     'status' => '',
             },
        ];
};

Given qr/a collection of phone with invalid records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                     'pidm' => '567890',
                     'phoneType' => 'CELL',
                     'phoneDesc' => '',
                     'activityDate' =>'',
                     'phoneNumber' => '+1-!@#-(778)(086)',
                     'phoneNumberNational' => '',
                     'phoneNumberInternational' => '',
                     'sequenceNumber' => '1',
                     'isPrimary' => '',
                     'status' => '',
             },
        ];
};

Given qr/a collection of phone records to update in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                'pidm' => '987654',
                'activityDate' => '',
                'phoneNumber' => '+15139999999',
                'sequenceNumber' => '1',
                'phoneType' => 'LO',
                'phoneDesc' => '',
                'phoneNumberNational' => '',
                'phoneNumberInternational' => '',
                'isPrimary' => 'true',
                'status' => '',
             },
        ];

};

Given qr/a collection of phone records to delete in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                'pidm' => '123456',
                'activityDate' => '',
                'phoneNumber' => '+15136646124',
                'sequenceNumber' => '1',
                'phoneType' => 'LO',
                'phoneDesc' => '',
                'phoneNumberNational' => '',
                'phoneNumberInternational' => '',
                'isPrimary' => '',
                'status' => '',
             },
        ];

};