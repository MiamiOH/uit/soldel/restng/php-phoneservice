#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {

my $idData = loadDataFromFile('ids');
my $phoneData = loadDataFromFile('phone');
my $pidmData = loadDataFromFile('pidm');
my $telecodeData = loadDataFromFile('telecode');
foreach my $table (qw(SPRTELE SZBUNIQ SPRIDEN STVTELE)) {
    $bannerDbh->do(qq{
            delete from $table
        });
}

foreach my $record (@{$telecodeData}) {
        $bannerDbh->do(q{
            insert into stvtele (stvtele_code, stvtele_desc, stvtele_activity_date)
                values (?, ?, ?)
            }, undef, $record->{'STVTELE_CODE'}, $record->{'STVTELE_DESC'}, uc $record->{'STVTELE_ACTIVITY_DATE'}
            );
    }

foreach my $record (@{$idData}) {
        $bannerDbh->do(q{
            insert into szbuniq (szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id, szbuniq_staff_ind, szbuniq_undergrad_ind)
                values (?, ?, ?, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'banner_id'}, uc $record->{'uid'},
                uc $record->{'staff'}, uc $record->{'student'});
    }
foreach my $record (@{$pidmData}) {
        $bannerDbh->do(q{
            insert into SPRIDEN (SPRIDEN_PIDM, SPRIDEN_ID, SPRIDEN_LAST_NAME)
                values (?, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'banner_id'}, uc $record->{'last_name'});
    }
foreach my $record (@{$phoneData}) {
    $bannerDbh->do(q{
        insert into SPRTELE(SPRTELE_PIDM,SPRTELE_SEQNO,SPRTELE_TELE_CODE,SPRTELE_ACTIVITY_DATE,SPRTELE_PHONE_AREA,SPRTELE_PHONE_NUMBER,
            SPRTELE_PHONE_EXT,SPRTELE_STATUS_IND,SPRTELE_ATYP_CODE,SPRTELE_ADDR_SEQNO,SPRTELE_PRIMARY_IND)
            values (?, ?, ?, sysdate, ?, ?, ?, ?, ?, ?, ?)
        }, undef, $record->{'PIDM'}, $record->{'SEQNO'}, $record->{'TELE_CODE'}, $record->{'PHONE_AREA'}, $record->{'PHONE_NUMBER'}, $record->{'PHONE_EXT'},
         $record->{'STATUS_IND'}, $record->{'ATYP_CODE'}, $record->{'ADDR_SEQNO'}, $record->{'PRIMARY_IND'});
}
};

sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];

    open FILE, "../sql/sampleData/$fileName.txt" or die "Couldn't open ../sql/sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;

    my @fieldNames = split("\t", $names);
    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}