Feature: Delete phone record of a user
  As a developer using the phone API
  I want to delete the phone record of a user
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication for the phone resource
    Given a REST client
    When I make a DELETE request for /person/phone/v1/doej
    Then the HTTP status code is 401

  Scenario: Delete a new phone record for a user
    Given a REST client
    And a token for the PHONE_WS_USER user
    And a collection of phone records to delete in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a DELETE request for /person/phone/v1/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "200"

