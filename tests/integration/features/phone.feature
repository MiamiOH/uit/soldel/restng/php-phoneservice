# Test the basic api by querying api resources and services
Feature: Get an API response
  As a developer using the Phone API
  I want to get information from the API
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication to get the phone information
    Given a REST client
    When I make a GET request for /person/phone/v1/doej
    Then the HTTP status code is 401

  Scenario: Get the Phone Information without authorization
    Given a REST client
    And a token for the TEST_WS_USER user
    When I make a GET request for /person/phone/v1/doej
    Then the HTTP status code is 401

  Scenario: Get a phone record information
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 3 entry
    And I get entry 3 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element phoneType equal to "LO"
    And the subject has an element isPrimary equal to empty
    And the subject has an element phoneNumber equal to "+15139999999"
    And the subject has an element sequenceNumber equal to "3"

  Scenario: Get a phone record information with phonetype filter as local
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1/doej?phoneType=LO
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 2 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element phoneType equal to "LO"
    And the subject has an element isPrimary equal to empty
    And the subject has an element phoneNumber equal to "+15135248312"
    And the subject has an element sequenceNumber equal to "1"

  Scenario: Get a phone record information with status filter as active
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1/doej?status=active
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 2 entry
    And I get entry 2 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element phoneType equal to "RH"
    And the subject has an element isPrimary equal to empty
    And the subject has an element phoneNumber equal to "+15135248312"
    And the subject has an element sequenceNumber equal to "2"

  Scenario: Get a phone record information with hyphen in phone number and status filter as active
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1/souther?status=active&phoneType=CELL
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "876253"
    And the subject has an element phoneType equal to "CELL"
    And the subject has an element isPrimary equal to "true"
    And the subject has an element phoneNumber equal to "+15131234567"
    And the subject has an element sequenceNumber equal to "3"

  Scenario: Request get phone record without a pidm
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1
    Then the HTTP status code is 500

  Scenario: Request get phone record with incorrect pidm
    Given a REST client
    And a token for the PHONE_WS_USER user
    When I make a GET request for /person/phone/v1/howardt
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 0 entry

  Scenario: Get an Phone Information with self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/phone/v1/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And I get entry 3 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element phoneType equal to "LO"
    And the subject has an element isPrimary equal to empty
    And the subject has an element phoneNumber equal to "+15139999999"
    And the subject has an element sequenceNumber equal to "3"

  Scenario: Get an Phone Information with incorrect self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/phone/v1/smithd
    Then the HTTP status code is 401
