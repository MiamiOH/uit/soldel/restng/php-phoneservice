<?php

// @codeCoverageIgnoreStart

namespace MiamiOH\RestngPhoneService\Services;

class PhoneHelper
{

    public function getInstance()
    {
        $instance = \libphonenumber\PhoneNumberUtil::getInstance();

        return $instance;
    }

    public function nationalCode()
    {
        return \libphonenumber\PhoneNumberFormat::NATIONAL;
    }

    public function internationalCode()
    {
        return \libphonenumber\PhoneNumberFormat::INTERNATIONAL;
    }

}

// @codeCoverageIgnoreEnd
