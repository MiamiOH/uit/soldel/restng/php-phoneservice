<?php
/*
-----------------------------------------------------------
FILE NAME: Phone.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Jimmy Xu

DESCRIPTION:  The phone service is designed to get, update and insert phone records to banner sprtele table
When doing get, pidm or uniqueId is required
When doing post, pidm and sequence number is needed

De-dup: The service will first check if there is duplicate records before doing insert, in the event that
it finds duplicate records, the error message will be returned to consumer app

Authorization: authentication and authorization are handled by authorization token. The Service will treat
phone data as public but when people insert new records, the consumer app would need update right of person
project in Authman

Post without sequence number: if the record being posted does not have duplicate in database, and the consumer
app did not provide seqence number, the service will treat this a new valiad phone number and will add up one
to the current sequence number and perform insert

Parameter where in get: This is a where clause provided to developer for debuging purpose. It should not be used
by consumer app. The service currently returns all information about a person and more parameters can be added in
the future

Protected group: SPBPERS_CONFID_IND = 'Y'. We will not update/get/insert any data for this people. Error message will
not be sent out but the consumer app should know the feature of this service.

INPUT:
PARAMETERS: pidm, sequence number and uniqueId

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:
	SATURN.SPRTELE (SELECT, INSERT)
	SATURN.SPBPERS (SELECT)		

 */

namespace MiamiOH\RestngPhoneService\Services;

class Phone extends \MiamiOH\RESTng\Service
{

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    /** @var \MiamiOH\RestngPhoneService\Services\PhoneHelper $phoneHelper */
    private $phoneHelper;

    /** @var \libphonenumber\PhoneNumberUtil $phoneUtil */
    private $phoneUtil;

    private $filterTypeList = [];
    private $filterStatus = '';

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    /**
     * @param $phoneHelper
     */
    public function setPhoneHelper($phoneHelper)
    {
        $this->phoneHelper = $phoneHelper;
        $this->phoneUtil = $phoneHelper->getInstance();
    }

    /**
     * @param $pidms
     *
     * @return mixed
     */
    public function read($pidms)
    {
        if (is_scalar($pidms)) {
            $pidms = array($pidms);
        }

        $values = $pidms;
        $queryString = '
                SELECT sprtele_pidm, sprtele_tele_code, rowidtochar(sprtele.rowid) as row_id,
                  sprtele_activity_date, sprtele_primary_ind,
                  sprtele_phone_area, sprtele_phone_number, sprtele_intl_access,
                  sprtele_phone_ext, sprtele_seqno, sprtele_status_ind, stvtele_desc
                FROM SPRTELE inner join stvtele on (sprtele_tele_code = stvtele_code)
                WHERE sprtele_pidm IN (' . implode(',',
                array_fill(0, count($pidms), '?')) . ')';

        if (count($this->filterTypeList)) {
            $placeHolders = [];
            foreach ($this->filterTypeList as $type) {
                $values[] = $type;
                $placeHolders[] = '?';
            }
            $queryString .= '
                    AND sprtele_tele_code in (' . join(', ', $placeHolders) . ')';
        }

        if ($this->filterStatus) {
            // TODO account for date range when determining status
            switch ($this->filterStatus) {
                case 'active':
                    $queryString .= '
                    and sprtele_status_ind is null';
                    break;

                case 'inactive':
                    $queryString .= '
                    and sprtele_status_ind = \'I\'';
                    break;
            }
        }

        $queryString .= '
                ORDER BY sprtele_seqno
            ';

        $records = $this->dbh->queryall_array($queryString, $values);

        for ($i = 0; $i < count($records); $i++) {
            $records[$i] = $this->makeModelFromRecord($records[$i]);
        }

        $this->clearFilters();

        return $records;
    }

    /**
     * @param $typeList
     *
     * @return $this
     */
    public function filterType($typeList)
    {
        if (!is_array($typeList)) {
            $typeList = [$typeList];
        }

        $this->filterTypeList = $typeList;

        return $this;
    }

    /**
     * @param $status
     *
     * @return $this
     * @throws \Exception
     */
    public function filterStatus($status)
    {
        if (!in_array($status, ['active', 'inactive'])) {
            throw new \Exception('Invalid filter status: ' . $status);
        }

        $this->filterStatus = $status;

        return $this;
    }

    /**
     *
     */
    public function clearFilters()
    {
        $this->filterTypeList = [];
        $this->filterStatus = '';
    }

    /**
     * @param $phone
     */
    public function create($phone)
    {
        if (!(isset($phone['phoneNumber']) && $phone['phoneNumber'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing required Phone Number');
        }
        $model['phoneNumber'] = $phone['phoneNumber'];


        $parsedPhoneNumber = $this->phoneUtil->parse($model['phoneNumber'], null,
            null, true);


        if ($this->phoneUtil->isValidNumber($parsedPhoneNumber)) {
            $model['phoneNumberNational'] = $this->phoneUtil->format($parsedPhoneNumber,
                $this->phoneHelper->nationalCode());
            $model['phoneNumberInternational'] = $this->phoneUtil->format($parsedPhoneNumber,
                $this->phoneHelper->internationalCode());
        } else {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Incorrect Phone Number entered');
        }

        $userName = $this->getApiUser()->getUsername();
        $dataOrigin = "RESTng Phone Service";


        $numberParts = $this->explodePhoneNumber($phone['phoneNumber']);

        $query = '
                    declare
                    P_SEQ_NO sprtele.sprtele_seqno%TYPE := NULL;
                    BEGIN
                    GB_TELEPHONE.P_CREATE(
                    p_pidm => :P_PIDM,
                    p_tele_code => :P_TELE_CODE,
                    p_phone_area => :P_PHONE_AREA,
                    p_phone_number => :P_PHONE_NUMBER,
                    p_phone_ext => :P_PHONE_EXT,
                    p_status_ind => :P_STATUS_IND,
                    p_atyp_code => NULL,
                    p_addr_seqno => NULL,
                    p_primary_ind => NULL,
                    p_unlist_ind => NULL,
                    p_comment => NULL,
                    p_intl_access => :P_INTERNATIONAL,
                    p_data_origin => :P_DATA_ORIGIN,
                    p_user_id => :P_USER_ID,
                    p_ctry_code_phone => NULL,
                    p_seqno_out => :P_SEQNO_OUT,
                    p_rowid_out => :P_ROWID_OUT);
                    END;';

        $sth = $this->dbh->prepare($query);

        $sequenceNumber = null;
        $status = (isset($phone['status']) && $phone['status'] === 'inactive' && $phone['status'] !== '') ? 'I' : null;
        $rowId = null;
        $phoneExtension = $parsedPhoneNumber->getExtension();

        $sth->bind_by_name(':P_PIDM', $phone['pidm']);
        $sth->bind_by_name(':P_TELE_CODE', $phone['phoneType']);
        $sth->bind_by_name(':P_PHONE_AREA', $numberParts['areaCode']);
        $sth->bind_by_name(':P_PHONE_NUMBER', $numberParts['number']);
        $sth->bind_by_name(':P_PHONE_EXT', $phoneExtension);
        $sth->bind_by_name(':P_INTERNATIONAL', $numberParts['internationalNumber']);
        $sth->bind_by_name(':P_STATUS_IND', $status);
        $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
        $sth->bind_by_name(':P_USER_ID', $userName);
        $sth->bind_by_name(':P_SEQNO_OUT', $sequenceNumber, 100);
        $sth->bind_by_name(':P_ROWID_OUT', $rowId, 100);

        $sth->execute();

        $phone['sequenceNumber'] = $sequenceNumber;

        // Call this method to build phoneDesc, phoneNumberNational and
        // phoneNumberInternational into the post response model
        $phone = $this->makeModelForPostResponse($phone);


        return $phone;
    }

    /**
     * @param $phone
     *
     * @return mixed
     */
    /* Using Update to change the status_ind to inactive . Using it as a substitute for Delete API as we are not authorized
    to delete phone records.*/
    public function update($phone)
    {


        $numberParts = $this->explodePhoneNumber($phone['phoneNumber']);

        $userName = $this->getApiUser()->getUsername();
        $dataOrigin = "RESTng Phone Service";


        $query = '
                    declare
						P_SEQ_NO sprtele.sprtele_seqno%TYPE := NULL;
						BEGIN
						GB_TELEPHONE.P_UPDATE(
						p_pidm => :P_PIDM,
						p_seqno => :P_SEQNO,
       					p_tele_code => :P_TELE_CODE,
       					p_phone_area => :P_PHONE_AREA,
						p_phone_number => :P_PHONE_NUMBER,
       					p_phone_ext => NULL,
       					p_status_ind => :P_STATUS_IND,
       					p_atyp_code => NULL,
       					p_addr_seqno => NULL,
       					p_primary_ind => NULL,
       					p_unlist_ind => NULL,
       					p_comment => NULL,
                        p_intl_access => :P_INTERNATIONAL,
       					p_data_origin => :P_DATA_ORIGIN,
       					p_user_id => :P_USER_ID,
       					p_ctry_code_phone => NULL,
       					p_rowid => :P_ROWID);
                    END;';

        $sth = $this->dbh->prepare($query);

        $status = empty($phone['status']) || $phone['status'] === 'active' ? null : 'I';

        $rowId = null;

        $sth->bind_by_name(':P_PIDM', $phone['pidm']);
        $sth->bind_by_name(':P_TELE_CODE', $phone['phoneType']);
        $sth->bind_by_name(':P_PHONE_AREA', $numberParts['areaCode']);
        $sth->bind_by_name(':P_PHONE_NUMBER', $numberParts['number']);
        $sth->bind_by_name(':P_INTERNATIONAL', $numberParts['internationalNumber']);
        $sth->bind_by_name(':P_STATUS_IND', $status);
        $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
        $sth->bind_by_name(':P_USER_ID', $userName);
        $sth->bind_by_name(':P_SEQNO', $phone['sequenceNumber']);
        $sth->bind_by_name(':P_ROWID', $rowId, 100); // TODO try OCI_B_ROWID here

        $sth->execute();

        return $phone;
    }

    public function delete($phone)
    {
        if (!(isset($phone['pidm']) && $phone['pidm']
            && isset($phone['phoneType']) && $phone['phoneType']
            && isset($phone['sequenceNumber']) && $phone['sequenceNumber']) && !(isset($phone['id']) && $phone['id'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('either id or the combination of PIDM/phoneType/sequenceNumber are required to delete a phone record');
        }

        /* The GB_TELEPHONE Delete procedure is not being used as it deletes them from the database and we are not authorized
        to delete phone records. */

        if(isset($phone['id']) && $phone['id']) {
            $query = '
					declare
                    BEGIN
                    GB_TELEPHONE.P_DELETE(
                    p_pidm => NULL,
       				p_tele_code => NULL,
       				p_seqno => NULL,
       				p_rowid => :P_ROW_ID);
                    END;';
            $sth = $this->dbh->prepare($query);
            $sth->bind_by_name(':P_ROW_ID', $phone['id']);
        } else {
            $query = '
					declare
                    BEGIN
                    GB_TELEPHONE.P_DELETE(
                    p_pidm => :P_PIDM,
       				p_tele_code => :P_TELE_CODE,
       				p_seqno => :P_SEQNO,
       				p_rowid => NULL);
                    END;';
            $sth = $this->dbh->prepare($query);
            $sth->bind_by_name(':P_PIDM', $phone['pidm']);
            $sth->bind_by_name(':P_TELE_CODE', $phone['phoneType']);
            $sth->bind_by_name(':P_SEQNO', $phone['sequenceNumber']);
        }

        $sth->execute();

        return true;

        /*
          $values = [];
          $values[] = $phone['pidm'];
          $values[] = $phone['phoneType'];
          $values[] = $phone['sequenceNumber'];

        $queryString = 'update sprtele set sprtele_status_ind = \'I\', sprtele_activity_date = sysdate
                    where (sprtele_pidm = ? and sprtele_tele_code = ? and sprtele_seqno = ?)';


          $records = $this->dbh->perform($queryString, $values);

          return true;
        */

    }

    /**
     * @param $record
     *
     * @return array
     */
    // Public for now to facilitate unit testing. We should move all model handling
    // to a specific class.
    public function makeModelFromRecord($record)
    {
        $model = [];

        $phoneNumber = '';

        $intlAccess = preg_replace('/[\s\(\)+-]/', '',
            $record['sprtele_intl_access']);
        $areaCode = preg_replace('/[\s\(\)+-]/', '', $record['sprtele_phone_area']);
        $number = preg_replace('/[\s\(\)+-]/', '', $record['sprtele_phone_number']);

        $model['areaPart'] = $areaCode;
        $model['numberPart'] = $number;

        // Check if sprtele_intl_access is set and is not a string( sometimes sprtele_intl_access field may contain country name)
        if ($record['sprtele_intl_access'] && !preg_match('/[A-Za-z]/',
                $record['sprtele_intl_access'])) {

            /* if sprtele_phone_area and sprtele_phone_number set then concatenate sprtele_intl_access, sprtele_phone_area and
               sprtele_phone_number to generate phone number. If generated phone number exceeds max length for international number
               then use sprtele_intl_access as phone number( some case sprtele_phone_area+sprtele_phone_number will have duplicate of
                sprtele_intl_access)*/
            if ($record['sprtele_phone_area'] && $record['sprtele_phone_number']) {
                $phoneNumber = $intlAccess . $areaCode . $number;
                $phoneNumber = '+' . $phoneNumber;

                if (strlen($phoneNumber) > 15) {
                    $phoneNumber = $record['sprtele_intl_access'];
                }
            } else {
                $phoneNumber = $record['sprtele_intl_access'];
            }

        } // if sprtele_intl_access is not set and areaCode and number are in USA format then display +1 for USA country code
        elseif (preg_match('/\A\d{3}\z/', $areaCode) && preg_match('/\A\d{7}\z/',
                $number)) {

            $phoneNumber = '+1' . $areaCode . $number;

        } /* if sprtele_intl_access is not set and areaCode and number are not in USA format, then we assume that inetrnational
           phone number is stored in sprtele_phone_area + sprtele_phone_number. */
        else {
            $phoneNumber = '+' . $areaCode . $number;
        }

        $model['pidm'] = $record['sprtele_pidm'];
        $model['phoneType'] = $record['sprtele_tele_code'];
        $model['phoneDesc'] = $record['stvtele_desc'];
        $model['activityDate'] = $record['sprtele_activity_date'];
        $model['sequenceNumber'] = $record['sprtele_seqno'];
        $model['isPrimary'] = $record['sprtele_primary_ind'] === 'Y';
        $model['status'] = (isset($record['sprtele_status_ind']) && $record['sprtele_status_ind'] === 'I' ? 'inactive' : 'active');
        $model['ext'] = $record['sprtele_phone_ext'] ?? null;
        $model['id'] = $record['row_id'];

        $model['phoneNumber'] = $phoneNumber;

        try {

            $parsedPhoneNumber = $this->phoneUtil->parse($model['phoneNumber'], null,
                null, true);

            if ($this->phoneUtil->isValidNumber($parsedPhoneNumber)) {
                $model['phoneNumberNational'] = $this->phoneUtil->format($parsedPhoneNumber,
                    $this->phoneHelper->nationalCode());
                $model['phoneNumberInternational'] = $this->phoneUtil->format($parsedPhoneNumber,
                    $this->phoneHelper->internationalCode());
            } else {
                $model['phoneNumberNational'] = $model['phoneNumber'];
                $model['phoneNumberInternational'] = $model['phoneNumber'];
            }

        } catch (\libphonenumber\NumberParseException $e) {
            $model['phoneNumberNational'] = $model['phoneNumber'];
            $model['phoneNumberInternational'] = $model['phoneNumber'];
        }

        return $model;
    }

    /**
     * @param $number
     *
     * @return array
     */
    public function explodePhoneNumber($number)
    {
        $parts = [
            'areaCode' => '',
            'number' => '',
            'internationalNumber' => '',
        ];

        if (preg_match('/\+1(\d{3})(\d{7})/', $number, $matches)) {
            $parts['areaCode'] = $matches[1];
            $parts['number'] = $matches[2];
        } else {
            $parts['internationalNumber'] = $number;
        }

        return $parts;
    }

    public function makeModelForPostResponse($phone)
    {
        $values = [];
        $values[] = $phone['pidm'];
        $values[] = $phone['phoneType'];
        $values[] = $phone['sequenceNumber'];

        // Read the newly inserted record from database to build the model for post response.
        $queryString = 'SELECT sprtele_pidm, sprtele_tele_code, rowidtochar(sprtele.rowid) as row_id,
            sprtele_activity_date, sprtele_primary_ind,
            sprtele_phone_area, sprtele_phone_number, sprtele_intl_access,
            sprtele_phone_ext, sprtele_seqno, sprtele_status_ind, stvtele_desc
            FROM SPRTELE inner join stvtele on (sprtele_tele_code = stvtele_code)
            WHERE sprtele_pidm = ? and sprtele_tele_code = ? and sprtele_seqno = ? and sprtele_status_ind is null';

        $records = $this->dbh->queryall_array($queryString, $values);

        // queryall_array returns only one record which is been created in POST
        $phone = $this->makeModelFromRecord($records[0]);

        return $phone;

    }

}
