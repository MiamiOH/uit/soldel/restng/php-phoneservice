<?php

namespace MiamiOH\RestngPhoneService\Services;

use MiamiOH\RESTng\App;

class PhoneREST extends \MiamiOH\RESTng\Service
{

    /** @var \MiamiOH\RestngPhoneService\Services\Phone $phone */
    private $phone;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    private $dbh;
    private $dataSourceName = 'MUWS_GEN_PROD';

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    public function setPhone($phone)
    {
        /** @var \MiamiOH\RestngPhoneService\Services\Phone $phone */
        $this->phone = $phone;
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function getPhone()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        try {
            $pidm = $this->getValidPidm($request);

            if (isset($options['status']) && $options['status']) {
                $this->phone->filterStatus($options['status']);
            }

            if (isset($options['phoneType']) && $options['phoneType']) {
                $this->phone->filterType($options['phoneType']);
            }

            $payload = $this->phone->read($pidm);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        } catch (\MiamiOH\RestngPhoneService\Exceptions\PidmNotFoundException $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $createPhoneResult = [
                'status' => \MiamiOH\RESTng\App::API_NOTFOUND,
                'message' => 'Pidm not found',
                'address' => [],
            ];
            $payload[] = $createPhoneResult;
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }
        $response->setPayload($payload);

        return $response;

    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function getPhoneCollection()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $pidms = $options['pidm'];

        // Refuse to return full collection; pidms filter must be set
        if (!isset($options['pidm'])) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        try {
            if (isset($options['status']) && $options['status']) {
                $this->phone->filterStatus($options['status']);
            }

            if (isset($options['phoneType']) && $options['phoneType']) {
                $this->phone->filterType($options['phoneType']);
            }

            $payload = $this->phone->read($pidms);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;

    }

    /*
     * We really don't have a way to represent a single phone object for create,
     * update or delete. The current model and implementation simply don't allow
     * for that. Therefore, create, update and delete will work on collections,
     * even if a single phone object is being acted on.
     */

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function postPhoneCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $phones = $request->getData();

        try {
            $pidm = $this->getValidPidm($request);
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } catch (\MiamiOH\RestngPhoneService\Exceptions\PidmNotFoundException $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $createPhoneResult = [
                'status' => \MiamiOH\RESTng\App::API_NOTFOUND,
                'message' => 'Pidm not found',
                'address' => [],
            ];
            $payload[] = $createPhoneResult;
        }

        if (isset($pidm)) {
            $payload = [];

            for ($i = 0; $i < count($phones); $i++) {
                if (!isset($phones[$i]['pidm'])) {
                    $phones[$i]['pidm'] = $pidm;
                }

                $createPhoneResult = [
                    'status' => \MiamiOH\RESTng\App::API_CREATED,
                    'message' => $phones[$i]['phoneNumber'] . " added to PIDM:" . $phones[$i]['pidm'],
                    'phone' => [],
                ];

                if ((int)$phones[$i]['pidm'] !== (int)$pidm) {
                    $this->log->info('Resource ID does not match PIDM in phone model');
                    $createPhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $createPhoneResult['message'] = 'Resource ID does not match PIDM in phone model';
                } else {
                    try {
                        $createPhoneResult['phone'][] = $this->phone->create($phones[$i]);
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $createPhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $createPhoneResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $createPhoneResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $createPhoneResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $createPhoneResult;
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        }
        $response->setPayload($payload);

        return $response;
    }

    private function getValidPidm($request)
    {
        if ($request->getResourceParamKey('muid') === 'pidm') {
            $pidm = $this->dbh->queryfirstrow_assoc("select SPRIDEN_PIDM from spriden where SPRIDEN_PIDM = ?",
                $request->getResourceParam('muid'));
            if (isset($pidm['spriden_pidm'])) {
                return $pidm['spriden_pidm'];
            } else {
                throw new \MiamiOH\RestngPhoneService\Exceptions\PidmNotFoundException('Pidm not found');
            }
        } else {
            $bannerId = $this->bannerUtil->getId(
                $request->getResourceParamKey('muid'),
                $request->getResourceParam('muid')
            );
            return $bannerId->getPidm();
        }
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function putPhoneCollection()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $phones = $request->getData();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        if (isset($pidm)) {
            $payload = [];

            $message = '';
            for ($i = 0; $i < count($phones); $i++) {
                $message = $message . 'Updated ' . $phones[$i]['phoneNumber'] . ' with  PIDM: ' . $phones[$i]['pidm'] . ' Sucessfully.';

                $updatePhoneResult = [
                    'status' => \MiamiOH\RESTng\App::API_OK,
                    'message' => $message,
                    'phone' => []
                ];
                $message = $message . '\n';
                if (!isset($phones[$i]['pidm'])) {
                    $phones[$i]['pidm'] = $pidm;
                }

                if ((int)$phones[$i]['pidm'] !== (int)$pidm) {
                    $this->log->info('Resource ID does not match PIDM in phone model');
                    $updatePhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $updatePhoneResult['message'] = 'Resource ID does not match PIDM in phone model';
                } else {
                    try {
                        $updatePhoneResult['phone'][] = $this->phone->update($phones[$i]);
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $updatePhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $updatePhoneResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $updatePhoneResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $updatePhoneResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $updatePhoneResult;
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);
        }

        return $response;

    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function deletePhoneCollection()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $phones = $request->getData();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        if (isset($pidm)) {
            $payload = [];

            $message = '';
            for ($i = 0; $i < count($phones); $i++) {
                $message = $message . $phones[$i]['phoneNumber'] . ' deleted from PIDM: ' . $phones[$i]['pidm'] . ' Sucessfully.';
                $deletePhoneResult = [
                    'status' => \MiamiOH\RESTng\App::API_OK,
                    'message' => $message,
                    'phone' => []
                ];
                $message = $message . '\n';
                if (!isset($phones[$i]['pidm'])) {
                    $phones[$i]['pidm'] = $pidm;
                }

                if ((int)$phones[$i]['pidm'] !== (int)$pidm) {
                    $this->log->info('Resource ID does not match PIDM in phone model');
                    $deletePhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $deletePhoneResult['message'] = 'Resource ID does not match PIDM in phone model';
                } else {
                    try {
                        if ($this->phone->delete($phones[$i])) {
                            $deletePhoneResult['phone'][] = $phones[$i];
                        }
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $deletePhoneResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $deletePhoneResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $deletePhoneResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $deletePhoneResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $deletePhoneResult;
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        }

        return $response;

    }

    public function deletePhoneById()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $data = [];
        $status = App::API_OK;

        try {
            if (!$this->phone->delete(['id' => $id])) {
                $data['message'] = 'Failed to delete phone (ID: ' . $id . ').';
                $status = App::API_BADREQUEST;
            }
        } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
            $status = App::API_BADREQUEST;
            $data['message'] = $e->getMessage();
        } catch (\Exception $e) {
            $status = App::API_FAILED;
            $data['message'] = $e->getMessage();
        }

        $response->setPayload($data);
        $response->setStatus($status);

        return $response;
    }
}