<?php

namespace MiamiOH\RestngPhoneService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class PhoneResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Phone',
            'type' => 'object',
            'properties' => array(
                'id' => [
                    'type' => 'string' // use ROW ID as resource ID
                ],
                'pidm' => array(
                    'type' => 'number',
                ),
                'phoneType' => array(
                    'type' => 'string',
                ),
                'phoneDesc' => array(
                    'type' => 'string',
                ),
                'activityDate' => array(
                    'type' => 'date',
                ),
                'isPrimary' => array(
                    'type' => 'boolean',
                ),
                'phoneNumber' => array(
                    'type' => 'string',
                ),
                'phoneNumberNational' => array(
                    'type' => 'string',
                ),
                'phoneNumberInternational' => array(
                    'type' => 'string',
                ),
                'sequenceNumber' => array(
                    'type' => 'number',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'ext' => [
                    'type' => 'string'
                ],
                'areaPart' => array(
                    'type' => 'string',
                ),
                'numberPart' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Phone.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Phone'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Phone.PostResponse',
            'type' => 'object',
            'properties' => array(
                'status' => array(
                    'type' => 'number',
                ),
                'message' => array(
                    'type' => 'string',
                ),
                'phone' => array(
                    'type' => 'object',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Phone.PostResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Phone.PostResponse'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonPhonePhoneHelper',
            'class' => 'MiamiOH\RestngPhoneService\Services\PhoneHelper',
            'description' => 'Phone number parsing, validation and formatting.',
        ));

        $this->addService(array(
            'name' => 'Phone',
            'class' => 'MiamiOH\RestngPhoneService\Services\Phone',
            'description' => 'Provide the phone information for the given uid.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'phoneHelper' => array(
                    'type' => 'service',
                    'name' => 'PersonPhonePhoneHelper'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PhoneREST',
            'class' => 'MiamiOH\RestngPhoneService\Services\PhoneREST',
            'description' => 'Provide the phone information for the given uid.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'phone' => array('type' => 'service', 'name' => 'Phone'),
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(

                'action' => 'update',
                'name' => 'person.phone.update',
                'description' => "Update a person's phone number",
                'pattern' => '/person/phone/v1/:muid',
                'service' => 'PhoneREST',
                'method' => 'putPhoneCollection',
                'isPageable' => false,
                'tags' => array('Person'),
                'returnType' => 'collection',
                'params' => array(
                    'muid' => array(
                        'description' => 'A Miami identifier',
                        'alternateKeys' => ['uniqueId', 'pidm']
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        )
                    ),
                    'authorize' => array(
                        array(
                            'type' => 'self',
                            'param' => 'muid',
                        ),
                        array(
                            'application' => 'WebServices',
                            'module' => 'Person-Phone',
                            'key' => 'update',
                        ),
                    ),
                ),
                'body' => array(
                    'description' => 'A phone record to be updated',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/Person.Phone.Collection'
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Successfully updated the status',
                    ),
                )
            )
        );

        $this->addResource(array(

            'action' => 'create',
            'name' => 'person.phone.post',
            'description' => 'Create new phone number',
            'pattern' => '/person/phone/v1/:muid',
            'service' => 'PhoneREST',
            'tags' => array('Person'),
            'method' => 'postPhoneCollection',
            'params' => array(
                'muid' => array(
                    'description' => 'A Miami identifier',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Phone',
                        'key' => 'create',
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),
            'returnType' => 'collection',
            'body' => array(
                'description' => 'A phone object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Phone.Collection'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Status code indicating status of phone creation',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Phone.PostResponse.Collection',
                    )
                ),
            )

        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'person.phone.delete',
            'description' => 'Delete a person phone resource',
            'tags' => array('Person'),
            'pattern' => '/person/phone/v1/:muid',
            'service' => 'PhoneREST',
            'method' => 'deletePhoneCollection',
            'params' => array(
                'muid' => array(
                    'description' => 'A Miami identifier',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Phone',
                        'key' => 'create',
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A collection of phone to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Phone.Collection'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Status code indicating status of phone deletion',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Phone.PostResponse.Collection',
                    )
                ),
            )
        ));

        $this->addResource([
            'action' => 'delete',
            'name' => 'person.phone.delete.id',
            'description' => 'Delete a person phone resource by ID',
            'tags' => ['Person'],
            'pattern' => '/person/phone/v1/id/:id',
            'service' => 'PhoneREST',
            'method' => 'deletePhoneById',
            'params' => [
                'id' => [
                    'description' => 'Phone ID'
                ],
            ],
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Phone',
                        'key' => 'create',
                    )
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Status code indicating status of phone deletion'
                ),
            )
        ]);

        $this->addResource(array(

            'action' => 'read',

            'name' => 'person.phone.v1.uid',

            'description' => 'Read person\'s phone number',

            'pattern' => '/person/phone/v1/:muid',

            'service' => 'PhoneREST',

            'tags' => array('Person'),

            'method' => 'getPhone',
            'params' => array(
                'muid' => array(
                    'description' => 'A Miami identifier',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),

            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Phone',
                        'key' => 'view'
                    ),
                )
            ),

            'returnType' => 'collection',

            'options' => array(
                'latest' => array(
                    'enum' => array('yes', 'no'),
                    'default' => 'no',
                    'description' => 'Display only the latest active phone number',
                ),
                'status' => array(
                    'enum' => array('active', 'inactive'),
                    'description' => 'Display only active or inactive phone numbers (default is to show all numbers)',
                ),
                'phoneType' => array(
                    'enum' => array('CELL', 'MA', 'RH', 'LO'),
                    'description' => 'Display phone numbers based on type',
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Status code indicating status of phone retrieval',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Phone.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.phone.v1',
            'description' => 'Read person\'s phone number',
            'pattern' => '/person/phone/v1/',
            'service' => 'PhoneREST',
            'tags' => array('Person'),
            'method' => 'getPhoneCollection',
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Phone',
                        'key' => 'view',
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),

            'returnType' => 'collection',

            'options' => array(
                'pidm' => array(
                    'required' => true,
                    'type' => 'list',
                    'description' => 'pidm(s) to get address records'
                ),
                'latest' => array(
                    'enum' => array('yes', 'no'),
                    'default' => 'no',
                    'description' => 'Display only the latest and active phone number',
                ),
                'status' => array(
                    'enum' => array('active', 'inactive'),
                    'description' => 'Display active or all phone numbers',
                ),
                'phoneType' => array(
                    'enum' => array('CELL', 'MA', 'RH', 'LO'),
                    'type' => 'list',
                    'description' => 'Display phone numbers based on type',
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Status code indicating status of phone retrieval',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Phone.Collection',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}